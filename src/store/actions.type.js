
export const CATEGORY_INSERT = "insertCategory"
export const CATEGORY_UPDATE = "updateCategory"
export const CATEGORY_DELETE = "deleteCategory"
export const CATEGORY_LIST_PAGE = "lstPageCategory"
export const CATEGORY_FETCH = "fetchCategoryById"
export const CATEGORY_RESET_STATE = "resetStateCategory"


