import { Notification } from 'element-ui';
import { SET_NOTIFICATION } from '@/store/mutations.type'
const mutations = {
  [SET_NOTIFICATION](state, noti) {
    switch (noti.type) {
      case 'success':
        Notification.success({ title: noti.title, message: noti.message, type: noti.type });
        break;
      case 'warning':
        Notification.warning({ title: noti.title, message: noti.message, type: noti.type });
        break;
      case 'error':
        Notification.error({ title: noti.title, message: noti.message, type: noti.type });
        break;
      case 'info':
        Notification.info({ title: noti.title, message: noti.message, type: noti.type });
        break;
      default:
        Notification.info({ title: noti.title, message: noti.message, type: noti.type });
        break;
    }
  },
}

export default {
  mutations
}
