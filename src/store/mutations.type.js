export const REMOVE_AUTH = 'logOut'
export const SET_AUTH = 'setUser'
export const SET_ERROR = 'setError'
export const SET_TOKEN = 'setToken'
export const SET_NAME = 'setName'
export const SET_AVATAR = 'setAvatar'

export const SET_CATEGORY = 'setCategory' // call when fetch by id, [updated], [inserted]..
export const SET_CATEGORIES = 'setCategories' // call when fetch list category
export const RESET_STATE = 'resetModuleState'
export const SET_NOTIFICATION = 'setShowNotification'
