import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import auth from './modules/auth'
import user from './modules/user'
import category from './modules/category'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    auth,
    user,
    category
  },
  getters
})

export default store
