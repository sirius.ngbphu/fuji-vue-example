import Vue from 'vue'
import router from '@/router'
import { MessageBox } from 'element-ui'

import { CategoryService, AuthService } from '@/api/service'
import JwtService from '@/utils/jwt.service'

import {
  CATEGORY_INSERT, CATEGORY_UPDATE, CATEGORY_DELETE, CATEGORY_LIST_PAGE, CATEGORY_FETCH, CATEGORY_RESET_STATE
} from '@/store/actions.type'
import { SET_CATEGORIES, SET_CATEGORY, SET_ERROR, RESET_STATE, SET_NOTIFICATION } from '@/store/mutations.type'


// const dùng để khởi tạo 1 đối tượng category
const initialState = {
  category: {
    name: {},
    code: '',
    description: ''
  },
  categories: [],
  errors: null,
  // user: {},
  // isAuthenticated: !!JwtService.getToken()
}

export const state = { ...initialState }

const actions = {
  async[CATEGORY_INSERT](context, category) {
    try {
      let res = await CategoryService.insert(category)
      // context.commit(SET_CATEGORY, res.data.data)
      context.commit(SET_NOTIFICATION, { type: 'success', title: 'SUCCESS', message: 'Add catergory successfully' })
      router.replace({ path: '/category/' + res.data.data })
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Add catergory failed' })
    }
  },
  async[CATEGORY_UPDATE](context, category) {
    try {
      let res = await CategoryService.update(category)
      context.commit(SET_CATEGORY, res.data.data)
      context.commit(SET_NOTIFICATION, { type: 'success', title: 'SUCCESS', message: 'Update catergory successfully' })
      // router.replace({path: '/categories'})
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Update catergory failed' })
    }
  },
  async[CATEGORY_DELETE](context, category) {
    try {
      debugger
      const { data } = await CategoryService.delete(category)
      context.commit(SET_NOTIFICATION, { type: 'success', title: 'SUCCESS', message: 'Delete catergory successfully' })
      context.dispatch(CATEGORY_LIST_PAGE)
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Update catergory failed' })
    }
  },
  async[CATEGORY_FETCH](context, id) {
    try {
      let res = await CategoryService.get(id)
      context.commit(SET_CATEGORY, res.data.data)
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Fetch catergories failed' })
    }
  },
  async[CATEGORY_LIST_PAGE](context, params) {
    try {
      
      let res = await CategoryService.queryByPage(params)
      context.commit(SET_CATEGORIES, res.data.data.results)
      debugger
      return res.data.data.total
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Fetch catergories failed' })
    }
  },
  [CATEGORY_RESET_STATE]({ commit }) {
    commit(RESET_STATE)
  }
}

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error
  },
  [SET_CATEGORY](state, category) {
    state.category = category
  },
  [SET_CATEGORIES](state, categories) {
    state.categories = categories
  },
  [RESET_STATE]() {
    for (let f in state) {
      Vue.set(state, f, initialState[f])
    }
  },
  [SET_NOTIFICATION](state, noti) {
    // type: success, warning, error, default
    // debugger
    // MessageBox.$message({
    //   showClose: true,
    //   message: noti.message,
    //   type: noti.type
    // })
    Vue.notify({
      group: 'notification-group',
      title: `${noti.title || 'Title'}`,
      type: `${noti.type || 'information'}`,
      text: `${noti.message || 'Message'}`,
      duration: `${noti.duration || 10000}`,
      speed: `${noti.speed || 1000}`

    })
  },
}
const getters = {
  // currentUser(state) {
  //   return state.user
  // },
  // isAuthenticated(state) {
  //   return state.isAuthenticated
  // },
  categories: state => {
    return state.categories
  },
  category: state => {
    return state.category
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}