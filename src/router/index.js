import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/categories',
      name: 'Categories',
      component: () => import('@/views/category/category'),
      meta: { title: 'Category', icon: 'table' }
      // children: [{
      //   path: 'categories',
      //   name: 'Categories',
      //   meta: { title: 'Category', icon: 'table' }
      // }]
    },
    {
      path: '/category',
      name: 'CategoryAdd',
      component: () => import('@/views/category/categoryForm'),
    },
    {
      path: '/category/:id',
      name: 'CategoryEdit',
      component: () => import('@/views/category/categoryForm')
    },
    // {
    //   path: '/',
    //   // component: Layout,
    //   redirect: '/category/',
    //   children: [{
    //     path: 'category',
    //     name: 'CategoryAdd',
    //     component: () => import('@/views/category/categoryForm'),
    //     hidden: true
    //   }]
    // },
    // {
    //   path: '/',
    //   // component: Layout,
    //   redirect: '/category/:id',
    //   children: [{
    //     path: 'category/:id',
    //     name: 'CategoryEdit',
    //     component: () => import('@/views/category/categoryForm'),
    //     hidden: true
    //   }]
    // },
  ]
})
