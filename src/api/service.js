import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL, DEV_URL } from "@/api/config";
import JwtService from "@/api/jwt.service";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = DEV_URL;
  },

  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${JwtService.getToken()}`;
  },

  query(resource, params) {
    debugger
    return Vue.axios.get(resource, params).catch(error => {
      debugger
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  get(resource, id = "") {
    return Vue.axios.get(`${resource}/${id}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, id, params) {
    return Vue.axios.put(`${resource}/${id}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const AuthService = {
  test() {
    return ApiService.get("test");
  },
  login(params) {
    return ApiService.post("login", params);
  }
};

export const CategoryService = {
  queryByPage(params) {
    return ApiService.post("category/get-by-page", params)
  },
  queryAll(params) {
    return ApiService.query("category/get-all", {
      params: params
    });
  },
  get(id) {
    return ApiService.get("category/detail?id=" + id)
  },
  insert(params) {
    return ApiService.post("category/insert", params)
  },
  update(params) {
    return ApiService.put("category/update", params)
  },
  delete(params) {
    return ApiService.post('category/delete', params)
  }
};
export const CategoryOldService = {
  queryByPage(params) {
    return ApiService.post("category/get-by-page", params)
  },
  queryAll(params) {
    return ApiService.query("category/get-all", {
      params: params
    });
  },
  get(id) {
    return ApiService.get("category/detail?id=" + id)
  },
  insert(params) {
    return ApiService.post("category/insert", params)
  },
  update(params) {
    return ApiService.put("category/update", params)
  },
  delete(params) {
    return ApiService.post('category/delete', params)
  }
};